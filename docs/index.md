# Jeux de société de la med

## Magic maze on Mars
Jeu de plateau coopératif à plusieurs niveaux et règles évolutives.

  - 2-6 joueurs
  - 20-40 minutes

Les premiers colons arrivent bientôt sur Mars, mais la colonie n’est pas encore
prête ! Incarnez des robots qui tentent de la construire ! Créez des ressources,
faites-les transiter à travers la planète et créez des ponts et des dômes. Mais
attention, chacun d’entre vous est assigné à une seule tâche… Il vous faudra
coopérer pour espérer finir dans les temps !

## Five Tribes
Jeu de plateau stratégique, il faut placer les bons pions au bon endroit et au
bon moment ! Les règles sont simples mais la stratégie complexe.

  - 2-4 joueurs
  - 1-2 heures

En traversant le pays des 1001 nuits, votre caravane arrive au légendaire
sultanat de Naqala. Le vieux sultan vient de mourir et le contrôle de Naqala est
à saisir ! Les oracles ont prédit des étrangers qui manœuvreraient les cinq
tribus pour gagner de l'influence sur la légendaire cité-État. Veux-tu accomplir
la prophétie ? Invoquez les vieux Djinns et mettez les tribus en position au bon
moment, et le Sultanat peut devenir le vôtre !

## Dice Forge
Jeu de plateau où vous pouvez vous-mêmes changer les faces de dé qui vous
octroieront les ressources nécessaires pour avancer.

  - 1 heures
  - 2-4 joueurs

Incarnez un héros gréco-romain : les dieux vous donnent la possibilité de
devenir le plus grand des mortels en forgeant vous-même les dés du destin.
Gagnez de l’or qui vous permettra de forger votre destinée, accomplissez des
exploits pour engranger des points et prouvez aux dieux qui vous êtes !

## King of Tokyo
Jeu de plateau défouloir ou le but est simplement de taper les autres à coups
de baffes et d’améliorations.

  - 2-6 joueurs
  - 30 minutes

À la tête d'un monstre mutant parfois de surcroît robotisé, vous devez être le
premier à détruire la ville de Tokyo (en obtenant 20 points de victoire) ou bien
être le dernier monstre en vie.

## Fuji
Jeu de dés coopératif, mais pas communicatif : vous ignorez ce que les autres
ont obtenu comme résultats.

  - 2-4 joueurs
  - 30 min

Dans Fuji, vous jouez un groupe d'aventuriers en route vers le volcan le plus
célèbre du Japon, le Mont Fuji. Mais juste avant d'arriver à destination, la
terre se met à trembler et le volcan entre en éruption ! Maintenant, votre
groupe doit échapper aux coulées de lave mortelles le plus rapidement possible
pour atteindre le village.

## Code Names
Jeu d’équipe – exercice de télépathie et de mise en relation de mots…

  - 4-beaucoup joueurs
  - 15 minutes

Vous incarnez des agents secrets qui cherchez cos contacts, mais une équipe
concurrente le fait également. Evitez de tomber sur l’assassin, ne perdez pas de
temps avec les civils innocents et trouvez votre équipe le plus vite possible en
fonction de ce que votre chef vous donne comme indices.

## Complots
Jeu de cartes politique dans la Renaissance.

  - 2-8 joueurs
  - 15 minutes

Une ville corrompue, soumise aux vices et avarices est sous le contrôle de vils
personnages. Le pouvoir est vacant, à vous de vous en emparer. Vous disposez en
secret de l’aide de deux personnages et par la ruse, la manipulation et le
bluff, vous n’aurez qu’une obsession : éliminer tous les autres de votre chemin.
À votre tour, vous pourrez user du pouvoir d’un des 6 personnages pour
espionner, soudoyer, prendre ou voler de l’argent et assassiner vos
adversaires...  Si personne ne remet en doute votre parole, vous pourrez
effectuer librement votre action, sinon un duel de bluff s’engagera et un seul
en sortira vivant !  Serez-vous le dernier ?

## Ninja Taisen
Jeu de cartes tactique.

  - 2 joueurs
  - 20 minutes

Ninja Taisen raconte l'histoire des villages connus sous les noms de Mashirazuka
et Yonakizawa. La sérénité berce ces deux villages, mais le traité de paix en
vigueur depuis une centaine d’années touche à sa fin. Les chefs de chaque
village, se haïssant, rassemblent et préparent leurs plus grands Ninjas pour
envahir le village ennemi. Qui sortira vainqueur de cette folle quête de
vengeance ?

## V Commandos
Jeu de plateau collaboratif et adaptatif avec diverses missions et divers
terrains.

  - 1-4 joueurs
  - 1h30

1941 . Hitler a soumis l’Europe occidentale et se trouve sur le point de mettre
l’URSS à genoux. Tout espoir semble perdu… Pourtant, un groupe d’individus
courageux, venus du monde entier, a choisi de ne pas ployer. Par des actions
éclair, menées au clair de lune et dans un silence implacable, ils assureront la
survie des peuples libres et contribueront à la victoire finale !

## Andor 3 : le dernier espoir
Jeu de plateau médiéval fantastique collaboratif.

  - 2-4 joueurs
  - 2 heures

Le dernier opus de la série !  Au terme de leurs expéditions criminelles, les
Kradhers avaient enlevé d’innombrables habitants du royaume d’Andor. Cependant,
les Héros étaient enfin de retour et bien décidés à libérer les prisonniers.
Sans tarder, vous vous lancez dans votre plus grande dans le sud du pays.  De
l’autre côté des Montagnes grises vous attendent des guerriers squelettes sans
pitié, sous les ordres de Kradhers géants. Les quatre Boucliers magiques vous
permettront-ils de surmonter ces terribles dangers ?  Vous êtes… le dernier
espoir du royaume.

## Mice and Mystics
Jeu de plateau évolutif par « chapitres ». Proche d’un JDR.

  - 1-4 joueurs
  - 1h20

Vous incarnez de loyaux chevaliers de votre Roi… Mais vous avez été transformés
en souris par un maléfice ! Vous devrez affronter cafards, rats, insectes et
même le chat du château pour espérer pouvoir sauver votre souverain !

## Solénia
Jeu de plateau coloré.

  - 1-4 joueurs
  - 45min

Depuis plusieurs millénaires, le cycle du jour et de la nuit s’est figé sur la
minuscule planète Solenia, plongeant son hémisphère nord dans une obscurité
permanente et son hémisphère sud dans une clarté ininterrompue. Votre mission
est de poursuivre la noble tâche de vos ancêtres en livrant aux habitants de la
planète les ressources qui leur font cruellement défaut. Les hommes du jour
attendent vos livraisons de pierre et d’eau, rares dans leur hémisphère, tandis
que les hommes de la nuit ont un besoin vital de bois et de blé. Soyez le plus
efficace et prenez de vitesse vos concurrents afin de posséder le plus d’étoiles
en or lorsque la partie prendra fin !

## Bataille pour Rokugan
Jeu de guerre proche du Risk dans l’univers de la Légende des 5 Anneaux. (Jeu
des éditions FFG, donc jeu génial)

  - 2-5 joueurs
  - 1-2 heures

Rokugan est une terre emplie d’esprits, de beauté, mais aussi de conflits. Dans
Bataille pour Rokugan, vous incarnez un daimyō, un chef sage et puissant qui
combat au nom de son clan pour gagner des terres et de l’honneur.  Les  daimyō
placent leurs troupes secrètement, en dissimulant leurs plans de bataille à
leurs adversaires. Chaque daimyō dispose, selon son clan, de capacités spéciales
et de troupes qu’il peut envoyer sur le champ de bataille. Il peut également
recruter des samurai loyaux, des éclaireurs agiles ou des shugenja maniant la
magie, qui lui permettront de renforcer ses stratégies.  Force, ruse et sagesse
vous conduiront à la victoire. Soyez le daimyō le plus honorable et prenez le
contrôle de terres de l’Empire d’Émeraude dans Bataille pour Rokugan !

## Saboteur
Jeu de cartes semi-coopératif branché détente.

  - 2-12 joueurs
  - 25 minutes

Les nains creusent la mine à la recherche de l’or, hélas des saboteurs se sont
sans doute dissimulés parmi eux ! Il leur faut atteindre le trésor au plus vite
en forant et en se méfiant de leurs collègues.

## Capital Lux
Jeu de cartes tactique.

  - 2-4 joueurs
  - 30 minutes

Dans un état très « steampunk », vous devez développer votre cité à l’aide de
marchands, notables, scientifiques et clercs. Mais la capitale elle aussi doit
être développée… Jouerez-vous des notables dans votre cité pour marquer des
points en fin de partie ou les placerez-vous dans la capitale pour bénéficier de
leurs capacités spéciales ?  Dans Capital Lux, vous êtes toujours sur le fil du
rasoir. Allez-vous prendre des risques ? Allez-vous bluffer ? Ou bien allez-vous
suivre un plan finement préparé ?

## Carcassonne
Jeu de « construction de plateau » légendaire. Juste inimitable.

  - 2-5 joueurs
  - 35 minutes

La cité de Carcassonne se développe dans le Sud de la France. Construisez
routes, villes et abbayes, et placez vos hommes au bon endroit pour vous assurer
la victoire.

## Citadelles
Jeu de cartes de développement.

  - 2-7 Joueurs
  - 45 min

Dans Citadelles, votre but est de bâtir une cité plus prestigieuse que vos
adversaires, le plus vite possible. Pour ceci, vous aurez besoin d’or, mais
aussi et surtout du soutien des nobles, des roturiers et des criminels pour vous
assurer la victoire.

## Diplomacy
Jeu de guerre sur plateau, sorte de Risk sans dés, qui laisse la part belle aux
négociations entre joueurs. Créé dans les années 50 . Maintes fois réédité (4
éditions différentes à la Med). Interminable.  Incroyable. Inimitable.

  - 2-beaucoup de joueurs
  - 3-8 heures

A l’aube du XXe siècle, l’Europe se prépare pour un nouveau conflit. Alors que
les armées se déplacent lentement et que des batailles dantesques se succèdent,
les diplomates ne ménagent pas leurs efforts pour former et dissoudre leurs
alliances, et s’assurer du contrôle des territoires les plus importants pour
ravitailler leurs armées.

## Pandemic et son extension
Jeu de coopération épique, qui, surprise, a connu une réédition cette année.

  - 2-5 joueurs
  - 45 minutes

4 maladies mortelles se sont déclarées partout sur la planète. Elles se
multiplient à un rythme alarmant, et l’OMS n’a d’autre choix que de créer une
équipe hétéroclite afin de les combattre. Endossez le rôle d’un médecin, d’un
chercheur ou d’un technicien et sauvez le monde ! Il faudra trouver les remèdes
et empêcher la propagation !  L’extension « Au seuil de la Catastrophe » propose
également des défis bien plus redoutables : une mutation incontrôlable, des
évènements infernaux, un mode légendaire et même un infâme bioterroriste.

## Wanted et Bang
Jeu de rôle caché.

  - 4-7 joueurs
  - 30 minutes

Dans une ville du Far West, un shérif, son adjoint, des bandits et un renégat
s’apprêtent à régler leurs comptes… Une chose est sûre, les colts vont parler !

## Mr Jack
Jeu asymétrique à deux joueurs, l'un doit échapper à l'autre, mais leurs pions
sont communs.

  - 2 joueurs
  - 45 minutes

Jack l’éventreur a été aperçu cette nuit dans le quartier de Whitechapel ! Les
sifflets des bobbys retentissent, le quartier est bouclé et les citoyens
s’apprêtent à l’appréhender… Tous ont des capacités bien différentes, mais l’un
d’entre eux doit forcément être le tueur en série !

## The Mind

  - 2-4 joueurs
  - 15 minutes

Jeu de coop, où il faut poser des cartes dans le bon ordre sans communiquer avec
ses partenaires.

## Hanabi
Jeu de coop où l'on voit uniquement la main des autres, et où il faut les guider
pour les faire poser les bonnes cartes.

  - 2-5 joueurs
  - 30 minutes

Le grand feu d’artifice impérial est sur le point de commencer ! Mais encore
faut-il que les artificiers puissent se coordonner suffisamment bien pour que
les fusées soient tirées au bon moment !

## Oui, Seigneur des Ténèbres
Jeu d'ambiance consistant à l’invention d’histoire.

  - 4-beaucoup de joueurs
  - 30 minutes

Après un n-ième échec, les gobelins au service de Rigor Mortis, le Seigneur des
ténèbres, se retrouvent à devoir expliquer leurs erreurs à leur patron. Mais
celui-ci a décidé de sévir, cette fois-ci, devant leur nullité ! Un seul espoir
pour eux : inventer faribole sur faribole et rejeter la faute sur les autres…

## Munchkin
Jeu d’ambiance détente dans l’univers du JDR.

  - 3-8 joueurs
  - 60 minutes

Souvenez-vous : vous rêviez de monstres à trucider, de dragons à étriller, de
princesses à sauver et de trésors à piller… Mais la dure réalité vous apparaît
enfin : les armuriers vous arnaquent, vos compagnons d’arme sont des pitres, les
monstres sont tous au courant que vous êtes niveau 1… et toujours pas la moindre
pièce d’or à l’horizon.

## Loup-garou one night
Jeu d’ambiance dont le but est de retrouver la succession d’évènements de « la
nuit ». Court et efficace.

  - 3-10 joueurs
  - 10 minutes

Dans ce jeu de cartes, chaque joueur joue le rôle d’un Villageois, d’un
Loup-garou ou d’un personnage spécial. C’est à vous de deviner qui sont les
Loups-garous et de tuer au moins l’un d’eux pour gagner... à moins que vous ne
soyez vous-même devenu un Loup-garou !  Un jeu avec un air de déjà vu ? Oui,
nous l'avons pensé aussi mais, détrompez-vous, celui-là est bien meilleur que
ses prédécesseurs ! Pas d'élimination directe (tout se passe en une seule nuit),
ni de modérateur.

## Petits meurtres et faits divers
Jeu d'ambiance dans lequel un détective doit interroger des suspects, et trouver
le coupable qui n'aura pas dit les mots obligatoires des innocents.

  - 4-7 joueurs
  - 45 minutes

Un crime des plus surprenants. Des innocents prêts à tout pour être accusés à
tort. Un criminel qui fera tout pour échapper à la Justice. Un inspecteur
déterminé, qui cherche à coincer le coupable. Il dispose de quelques
interrogatoires pour déterminer le coupable… Son alibi diverge-t-il de la
version des autres ?

## Dominion
Jeu de deck building.

  - 2-4 joueurs
  - 45 minutes

A la tête d’un domaine, avec vos quelques pièces de cuivre, vous vous lancez
dans une course au développement contre d’autres seigneurs. Il vous faudra
engager des personnels, créer des infrastructures, acheter des terres et prendre
garde à vos adversaires…

## Aventuriers du rail (Europe + USA)
Jeu de plateau légendaire.

  - 2-5 joueurs
  - 45 minutes

Fin du XXe siècle. L’ère des chemins de fer et des locomotives à vapeur a
sonnée. A la tête de votre compagnie, vous devez relier des villes et bâtir des
lignes afin de surclasser vos adversaires et de satisfaire vos actionnaires. Des
quais brumeux de Pampelonne aux rues glacées de St Pétersbourg, des plaines du
Montana aux buildings de New-York, vous devrez mériter votre titre de baron du
Rail ! Achetez votre billet, hélez un porteur et sautez dans le prochain train !

## Karmaka
Jeu de cartes stratégique.

  - 2-4 joueurs
  - 30-60 minutes

Karmaka est un jeu de cartes stratégique qui vous immerge dans un univers
karmique. Les joueurs commencent en tant que scarabées bousiers. Vie après vie,
main après main (de cartes), ils griment l'échelle karmique, pour essayer d'être
le premier à atteindre la Transcendance.

## Room25
Jeu de plateau semi-coopératif.

  - 1-8 joueurs
  - 45 minutes

Dans un futur proche, un jeu télévisé nommé Room 25 franchit les limites de
l’insoutenable pour battre des records d’audience. Des candidats sont enfermés
dans un complexe carré doté de 25 salles en mouvement truffées de pièges. Ils
devront essayer de se faire confiance pour rejoindre ensemble la Room 25 et
s’échapper rapidement.  Dans certains cas, des gardiens pourront s’infiltrer
dans le groupe et tenter de les empêcher de fuir par tous les moyens à leur
disposition…

## Les naufragés du Titanic
Jeu d’ambiance ayant inspiré une fameuse murder. Faites tout de même attention
avec qui vous jouez.

  - 4-6 joueurs
  - 45 minutes

Vous dérivez sur un canot de sauvetage avec votre amour secret, votre pire
ennemi et des réserves d’eau insuffisantes.  Qu’est-ce qui pourrait bien vous
arriver de pire ?

## Love letters

  - 2-6 joueurs

Jeu très court qui ne mérite même pas qu’on fasse un résumé de celui-ci.
Dites-vous qu’en plus il est répétable à l’infini… A l’infini… A l’infini… A
l’infini… A l’infini… A l’infini… A l’infini… A l’infini… A l’infini… A
l’infini… A l’infini… A l’infini… A l’infini… A l’infini… A l’infini… A
l’infini… A l’infini…

## Welcome back to the dungeon
Jeu de stop ou encore.

  - 2-4 joueurs
  - 30 minutes

Depuis que 4 inconscients se sont essayés à entrer dans le terrible Donjon du
bois Sans Retour, l’histoire a fait le tour du royaume. Nombreux sont alors les
fous qui ont voulu reproduire cet exploit, sans jamais en ressortir vivants…
enfin, jusqu’à aujourd’hui !  4 nouveaux aventuriers se dressent face à ce
donjon, dont la réputation fait trembler dans les chaumières ! Fiers, fous ou
courageux, ils sont bien décidés à en découdre avec les habitants de ce terrible
lieu… ou peut-être pas tant que ça, finalement ! Ce qu’ils ignorent, c’est que
de nouveaux monstres encore plus puissants ont fait leur apparition et ont faim
… Très faim !  Dans ce jeu, tous les joueurs jouent avec le même héros, et
tentent d'être celui qui l'entraînera dans les profondeurs du donjon en quête de
trésors et de gloire. Placez l'Aventurier, objet de toutes vos convoitises, au
centre de la table, avec les 6 armes et protections qui composent son équipement
pour affronter les Monstres qui peuplent le Donjon.

## Skull
Jeu de bluff et d’intuition.

  - 3-6 joueurs
  - 30 minutes

Les matelots s’affrontent selon des règles ancestrales : qui sera capable de
retourner exactement le bon nombre de fleurs sans s’attirer la colère des crânes
des ancêtres ?

## 7 Wonders
Jeu de développement aux règles plutôt simples mais hautement stratégique. Les
extensions Leaders et Cities sont inclues dans la boîte que nous possédons.
Couronné de dizaines de prix internationaux, tout bonnement mythique et
incontournable.

  - 2-7 joueurs
  - 35 minutes

Vous voilà dirigeant de l’une des sept plus grandes cités de l’antiquité.
Développez votre cité en multipliant les découvertes scientifiques, les
conquêtes militaires, les accords commerciaux et les édifices prestigieux pour
mener votre civilisation vers la gloire ! Pendant ce temps, gardez à l’œil les
progrès de vos voisins qui partagent cette même ambition. Votre civilisation
arrivera-t-elle à transcender les millénaires à venir ?

## Splendor
Jeu de cartes léger mais très intéressant.

  - 2-4 joueurs
  - 30 minutes

Dans Splendor, vous dirigez une guilde de marchands dans la Renaissance. A
l’aide de jetons symbolisant des pierres précieuses, vous allez acquérir des
développements qui produisent de nouvelles pierres (les bonus). Ces bonus
réduisent le coût de vos achats et attirent les nobles mécènes comme François
Ier ou Ann Boleyn. Chaque tour est rapide : une action et une seule ! Observez
vos adversaires, anticipez et réservez les bonnes cartes.

## Cash’n Guns
Jeu d’ambiance marrant dans l’univers du film policier.

  - 4-6 joueurs
  - 30 minutes

Une bande de gangsters se partage le magot de leur dernier braquage dans un
entrepôt désaffecté, mais personne n'est d'accord sur les modalités du partage…
Entre bluffs, menaces et autres coups de théâtre… Faites en sorte de sortir avec
les biftons et pas les pieds devant ! 

## Timeline Inventions
Jeu court et instructif (ce qui ne peut être qu’un plus)

  - 2-9 joueurs
  - 15 minutes

L’Histoire est jalonnée d’inventions qui ont changé le monde, mais savez-vous
quant elles ont été découvertes ? L’invention de la poudre noire doit être
située bien avant celle de la calculatrice… Mais quid de la découverte d’Uranus
? L’invention des étriers ? Ou du savon ?

## Les Inventeurs
Jeu court et aux mécaniques simplistes. Mais relativement appréciable.

  - 2-4 joueurs
  - 30 minutes

Dans la pénombre de leurs laboratoires, les plus grands scientifiques de
l’Histoire collaborent pour changer la face du monde. En collaborant à des
projets et des développements, ils pourront poser des brevets, acquérir des
connaissances supplémentaires et développer leurs capacités. Choisissez la
meilleure équipe de tous les temps entre Antiquité, Moyen-Âge, Epoque Moderne et
Epoque contemporaine, et faites s’affronter Einstein, Archimède, Newton et
Fibonacci dans une joute épique par la recherche et pour la recherche !

## 6 qui prend
Jeu de cartes simple et rapide, mais assez prenant.

  - 2-10 joueurs
  - 30 minutes

Déposez vos bêtes à cornes dans la bonne rangée, et, surtout, n'en ramassez
aucune !

## Smallworld
Jeu de stratégie des éditions Days of Wonder (génial donc). Possédant de
nombreuses extensions, il est réellement intéressant et arrive à faire en sorte
que les parties soient très variables.

  - 2 à 5 joueurs
  - 1 heure 15

Dans Small World, les joueurs luttent pour conquérir les régions d'un monde où
il n'y a pas de place pour tous ! En choisissant la bonne combinaison entre les
14 Peuples et les 20 Pouvoirs Spéciaux au bon moment, les joueurs pourront
étendre leur empire, souvent aux dépens de leurs voisins ! Cependant, leur
civilisation finira par s'essouffler - il leur faudra alors en choisir une autre
pour remporter la victoire… Faites s’affronter Trolls des Montagnes, Magiciens
volants, Elfes et leur dragon et tritons scouts dans un monde décidément trop
petit !

## Intrigues à Venise
Jeu de tactique et d'espionnage dans une Venise de la Renaissance en plein
carnaval. Assez immersif et haletant.

  - 3 à 4 joueurs
  - 45 minutes

C'est le Carnaval à Venise. La nuit est pleine d'animation et de sons joyeux, de
voix exubérantes, des rires et des chansons de personnages masqués qui dansent
dans les ruelles étroites. Mais cette joyeuse ambiance cache aussi les activités
de quatre sinistres personnages, quatre maîtres du trompe-l'œil capables de se
déguiser à tel point qu'ils sont impossibles à reconnaître...

## Abyss
Jeu de plateau stratégique merveilleusement bien illustré.

  - 2 à 4 joueurs
  - 45 minutes

Le trône du royaume sous-marin est vacant, et les abysses cherchent un nouveau
souverain. Il vous appartient de fédérer les peuples des Océans, et de vous
octroyer le soutien des différents militaires, diplomates, commerçants,
agriculteurs et magiciens qui peuplent la cour. Les lieux et les perles vous
procureront également plus de puissance pourvu que vous sachiez quand acheter
des soutiens à vos adversaires et quand récolter les perles, la monnaie des
abysses !

## Race for the Galaxy
Jeu de cartes de développement aux multiples extensions. Simple, efficace,
excellent.

  - 2 à 4 joueurs
  - 40 minutes

A la tête d’une planète de départ, il vous faudra développer des technologies
diverses et variées, explorer et coloniser de nouveaux mondes, et récolter vos
ressources au bon moment pour bâtir l’empire galactique le plus prestigieux !

## Colt Express
Jeu de plateau assez innovant dans le sens où les joueurs déplacent leur pion
sur un train en 3D.

  - 2 à 6 joueurs
  - 40 minutes

À la fin du XIX ème siècle, dans l’Ouest américain, les trains sont des cibles
convoitées. Hors-la-loi et mercenaires cherchent à dérober les richesses des
passagers et les cagnottes fédérales. De célèbres bandits comme Jesse James ou
Butch Cassidy construisent leur réputation lors des attaques des trains. Dans
Colt Express, chaque joueur incarne un bandit, prêt à tout. Son but : devenir le
hors-la-loi le plus riche de l’Ouest en dérobant les plus précieux biens à bord
du train et en abattant ses adversaires !

## Captain Sonar
Jeu par équipe épique imitant le fonctionnement d’un SNLE. Chaque personne dans
son équipe a un poste précis, et comme le jeu ne fonctionne pas par tours, il
n’y a pas vraiment de temps mort !

  - (2 à) 8 Joueurs
  - 40 minutes

Dans les ténèbres des fonds marins, deux sous-marins de puissances rivales
viennent de détecter la présence d’un adversaire. Branle-bas de combat, tout le
monde à son poste ! Capitaine, lieutenant, opérateur radar et technicien se
préparent à une bataille acharnée dans les abysses : il leur faudra repérer
l’adversaire, lancer torpilles et mines, naviguer entre les récifs, utiliser
leur énergie à bon escient, virer de bord et faire surface au bon moment !

## Clank
Jeu de plateau et de deck building, avec un soupçon de chance et de « stop ou
encore ». Un coktail très sympa.

  - 2 à 4 joueurs
  - 1 heure

Les souterrains sous la Tour du Dragon sont réputés pour être l’endroit le plus
dangereux du Royaume. Seuls les roublards les plus intrépides peuvent s’y
faufiler pour voler le Dragon et en revenir vivants pour raconter leur histoire.
C’est ainsi que vous et vos semblables avez mis en jeu votre réputation de
voleurs pour relever le défi. Au fil des tours, vous allez améliorer votre deck
et affûter votre stratégie. Serez-vous prudent ou téméraire ? Attention, les
meilleures cartes sont aussi les plus bruyantes, et plus vous allez faire de
boucan (CLANK!), plus vous risquez d’attirer la fureur du dragon…

Sachez sortir à temps, et surtout, sortez le plus riche !
